Mycroft
=======

Scripting the stock open Minecraft server libraries via Common Lisp.


Status
======

Scaffolding to get a persistent open Minecraft server running in the
same process space as ABCL with an open Swank connection exists. 

Colophon
========

    Mark <mevenson@common-lisp.net>
    Created: 02-FEB-2019
    Revised: <2019-02-06 Wed 15:32Z>
    
    
    
