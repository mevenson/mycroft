(in-package :mycroft.server)

;;; org.bukkit.craftbukkit.Main
(defun main/craftbukkit (&optional args)
  (ensure-dependencies/craftbukkit)
  (let ((args (java:jnew-array-from-array "java.lang.String"
                                          (if args
                                              `#(,@args)
                                              #("")))))
    (#"main" 'org.bukkit.craftbukkit.Main args)))


