(in-package :mycroft.server)

;;; org.bukkit.craftbukkit.Main
(defun main/spigot (&optional args)
  (ensure-dependencies/spigot)
  (let ((args (java:jnew-array-from-array "java.lang.String"
                                          (if args
                                              `#(,@args)
                                              #("")))))
    (#"main" 'org.bukkit.craftbukkit.Main args
