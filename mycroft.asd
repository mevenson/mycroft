#-abcl (error "You need the Bear for this one")
(defsystem mycroft :depends-on nil
           :components ((:module package :pathname "./"
                                 :components ((:file "package")))))

(defsystem mycroft/install
  :depends-on (mycroft)
  :components ((:module source :pathname "./"
                        :components ((:file "install")))))

(defsystem mycroft/minecraft_server.jar/1/13/2
    :defsystem-depends-on (abcl-asdf)
    :components ((:module jar-dependencies :pathname "./"
                          :components ((:jar-file "minecraft_server.1.13.2")))))

(defsystem mycroft/server/majong
  :depends-on (mycroft
               mycroft/minecraft_server.jar/1/13/2)
  :components ((:module source :pathname "./"
                        :depends-on (jar-dependencies) ;;; 
                        :components ((:file "majong")))))

(defsystem mycroft/craftbukkit.jar/1/13/2
    :defsystem-depends-on (abcl-asdf)
    :components ((:module jar-dependencies :pathname "./"
                          :components ((:jar-file "craftbukkit-1.13.2")))))

(defsystem mycroft/server/craftbukkit
  :depends-on (mycroft
               mycroft/craftbukkit.jar/1/13/2)
  :components ((:module source :pathname "./"
                        :components ((:file "craftbukkit")))))

(defsystem mycroft/spigot.jar/1/13/2
    :defsystem-depends-on (abcl-asdf)
    :components ((:module jar-dependencies :pathname "./"
                          :components ((:jar-file "spigot-1.13.2")))))

(defsystem mycroft/server/spigot
  :depends-on (mycroft
               mycroft/spigot.jar/1/13/2)
  :components ((:module source :pathname "./"
                        :components ((:file "spigot")))))

