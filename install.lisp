(in-package :mycroft.server)
(require :jss)

;;; FIXME: make this less brittle, handle newer versions more
;;; gracefully.
(defun ensure-dependencies/majong ()
  (let* ((system (asdf:find-system :mycroft/minecraft_server.jar/1/13/2))
         (root (asdf/interface:component-pathname system)))
    ;;; XXX move to initialization sequence
    (#"setProperty" 'System "user.dir"
                    (namestring root))
    (let* ((jar-component (asdf:find-component
                           (asdf:find-component system :jar-dependencies)
                           "minecraft_server.1.13.2"))
           (p (asdf/interface:component-pathname
               jar-component)))
      (values
       (if (not(probe-file p))
          ;;; uri sniffed from an interactive browser session in early Feb 2019
         (let ((uri "https://launcher.mojang.com/v1/objects/3737db93722a9e39eeada7c27e7aca28b144ffa7/server.jar"))
           (format *standard-output* "~&Downloading ~a to ~a.~%" uri p)
           (abcl/build:download uri :destination p))
         p)
       (ensure-eula root)))))

(defvar *var/* nil)
(defun var/ ()
  (if (null *var/*)
      (setf *var/*
            #+abcl
            (ext:make-temp-directory)
            #-abcl
            (error "Need definition of how to make a temporary directory in the implementation."))
      *var/*))

;;; 1. Download <https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar>
;;; 2. Close eyes.
;;; 3. Run its main method in a temporary directory.
(defun spigotmc-buildtools ()
  (let* ((uri "https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar")
         (p (make-pathname :defaults (var/)
                           :name (pathname-name uri)
                           :type (pathname-type uri))))
    (unless (probe-file p)
      (format *standard-output* "~&Downloading ~a to ~a.~%" uri p)
      (abcl/build:download uri :destination p))
    (set-directory (var/))
    (format *standard-output*
            (format nil "Invoking Java build process in ~a" (var/)))
    (uiop/run-program:run-program
     (format nil "cd ~a && java -jar ~a" (namestring (var/) (namestring p)))
     :output :string :error :string)))

(defun ensure-dependencies/craftbukkit ()
  (let* ((system (asdf:find-system :mycroft/craftbukkit.jar/1/13/2))
         (root (asdf/interface:component-pathname system)))
    (#"setProperty" 'System "user.dir"
                    (namestring root))
    (let* ((jar-component (asdf:find-component
                           (asdf:find-component system :jar-dependencies)
                           "craftbukkit-1.13.2"))
           (p (asdf/interface:component-pathname
               jar-component))
           (built-jar (make-pathname :defaults (var/)
                                     :name (pathname-name p)
                                     :type (pathname-type p))))
      (unless (probe-file p)
        (unless (probe-file built-jar)
          (spigotmc-buildtools))
        (uiop/stream:copy-file built-jar p)))))

(defun ensure-dependencies/spigot ()
  (let* ((system (asdf:find-system :mycroft/spigot.jar/1/13/2))
         (root (asdf/interface:component-pathname system)))
    (#"setProperty" 'System "user.dir"
                    (namestring root))
    (let* ((jar-component
            (asdf:find-component (asdf:find-component system :jar-dependencies)
                                 "craftbukkit-1.13.2"))
           (p
            (asdf/interface:component-pathname jar-component))
           (built-jar
            (make-pathname :defaults (var/)
                           :name (pathname-name p)
                           :type (pathname-type p))))
      (unless (probe-file p)
        (unless (probe-file built-jar)
          (spigotmc-buildtools))
        (uiop/stream:copy-file built-jar p)))))

(defun ensure-eula (root)
  (let ((eula (merge-pathnames "eula.txt" root)))
    (if (not (probe-file eula))
        (with-open-file (o eula :direction :output)
          (format o "eula=true~%"))
        eula)))

;;; Set the Common Lisp notion of directory as well as the JVM's
;;; notion of the current directory.
(defun set-directory (directory)
  (setf *default-pathname-defaults* directory)
  (#"setProperty" 'System "user.dir" (namestring directory)))

