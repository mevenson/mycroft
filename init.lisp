(in-package :cl-user)

(asdf:load-system :swank)

(defparameter *swank-port* 4005)

(defvar *swank-server-started-p* nil)
(unless *swank-server-started-p*
  (setf swank::*loopback-interface* "0.0.0.0") ;; N.b SECURITY This promiscuously binds to all interfaces
  (swank:create-server :port *swank-port* :dont-close t)
  (setq *swank-server-started* t))

;;; Enables the load of the minecraft server jar in our process space
;;; TODO how to instruct ASDF to find `mycroft.asd`
(asdf:load-system :mycroft)



