(defpackage mycroft
  (:use #:cl))

(defpackage mycroft.server
  (:nicknames #:mycroft-server #:server)
  (:use #:cl)
  (:export

   #:var/

   ;;; Consider not exporting these symbols, but they are usefull for
   ;;; manually running the process.
   #:spigotmc-buildtools
   #:ensure-eula

   #:ensure-dependencies/craftbukkit
   #:main/craftbukkit

   #:ensure-dependencies/spigot
   #:main/spigot
   
   #:ensure-dependencies/majong
   #:main/majong))





